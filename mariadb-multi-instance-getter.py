#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import subprocess
import re
import sys
maria_re = re.compile(r"mariadb@(\S+).service")
maria_port = re.compile(r"^port\s+\=\s+(\d+)\s+$")

def list_instances():
    service_list = subprocess.Popen("/usr/bin/systemctl", stdout=subprocess.PIPE)
    service_list, err = service_list.communicate()
    if err is not None:
        print(err)
    mariadb_instances = {}
    for service in service_list.decode().split("\n"):
        if "mariadb@" in service and maria_re.search(service):
            instance_name = maria_re.search(service).group(1)
            if instance_name is not None:
                mariadb_instances[instance_name] = {
                    'source': get_instance_source(instance_name),
                    'replication_port': get_instance_port(instance_name)
                    }
                if mariadb_instances[instance_name] == None:
                    print("Failed to retrieve master info")
                    sys.exit(1)
            else:
                print("Failed to retrieve instance name")
                sys.exit(1)
    return mariadb_instances

def get_instance_source(instance_name):
    socket = "/run/mysqld/mysqld.%s.sock" % instance_name
    mysql_cmd = ["sudo", "mysql", "-S", socket, "-e", "SHOW SLAVE STATUS\G"]
    master_coord = subprocess.Popen(mysql_cmd, stdout=subprocess.PIPE)
    master_coord, err = master_coord.communicate()
    if err is not None:
        print(err)
    for line in master_coord.decode().split("\n"):
        if "Master_Host:" in line:
            return line.split("Master_Host: ")[1]
    return None

def get_instance_port(instance_name):
    conf = "/etc/mysql/mysqld.conf.d/%s.cnf" % instance_name
    f = open(conf)
    for line in f.readlines():
        if maria_port.search(line):
            f.close()
            return maria_port.search(line).group(1)
    f.close()
    return None


if __name__ == "__main__":
    print(list_instances())